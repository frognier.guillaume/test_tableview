# CMake generated Testfile for 
# Source directory: /home/guillaume/Qt-creator/test_tableview/src
# Build directory: /home/guillaume/Qt-creator/test_tableview/build-src-Desktop-Debug
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(appstreamtest "/usr/bin/cmake" "-DAPPSTREAMCLI=/usr/bin/appstreamcli" "-DINSTALL_FILES=/home/guillaume/Qt-creator/test_tableview/build-src-Desktop-Debug/install_manifest.txt" "-P" "/usr/share/ECM/kde-modules/appstreamtest.cmake")
set_tests_properties(appstreamtest PROPERTIES  _BACKTRACE_TRIPLES "/usr/share/ECM/kde-modules/KDECMakeSettings.cmake;165;add_test;/usr/share/ECM/kde-modules/KDECMakeSettings.cmake;183;appstreamtest;/usr/share/ECM/kde-modules/KDECMakeSettings.cmake;0;;/home/guillaume/Qt-creator/test_tableview/src/CMakeLists.txt;15;include;/home/guillaume/Qt-creator/test_tableview/src/CMakeLists.txt;0;")
subdirs("table")
