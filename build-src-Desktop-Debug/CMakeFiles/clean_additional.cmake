# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "Debug")
  file(REMOVE_RECURSE
  "CMakeFiles/test_tableview_autogen.dir/AutogenUsed.txt"
  "CMakeFiles/test_tableview_autogen.dir/ParseCache.txt"
  "table/CMakeFiles/tableviewplugin_autogen.dir/AutogenUsed.txt"
  "table/CMakeFiles/tableviewplugin_autogen.dir/ParseCache.txt"
  "table/CMakeFiles/tableviewstatic_autogen.dir/AutogenUsed.txt"
  "table/CMakeFiles/tableviewstatic_autogen.dir/ParseCache.txt"
  "table/tableviewplugin_autogen"
  "table/tableviewstatic_autogen"
  "test_tableview_autogen"
  )
endif()
