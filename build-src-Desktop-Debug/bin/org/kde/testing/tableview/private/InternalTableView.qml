import QtQuick 2.15
import org.kde.qqc2desktopstyle.private 1.0 as StylePrivate

import QtQuick.Controls 2.15
import org.kde.kirigami 2.9 as Kirigami
import QtQml.Models 2.15
import org.kde.testing.tableview as Table

FocusScope {
    id: root

    property int sortColumn: 0

    property int sortOrder: Qt.AscendingOrder

    property bool sortIndicatorVisible: false

    property QtObject model
    property alias delegate: tableView.delegate

    signal sort(int column, int order)
    signal resize(int column, real width)
    signal contextMenuRequested(var index, point position)
    signal headerContextMenuRequested(int column, point position)

    property var columnWidthsRatios: []
    property real defaultColumnWidthRatio: 0.1
    property real minimumColumnWidth: Kirigami.Units.gridUnit * 4
    property real rowHeight: Kirigami.Units.gridUnit + Kirigami.Units.smallSpacing * 2

    readonly property alias selectionModel: tableView.selectionModel
    enum SelectionModeType {
        //NoSelection,
        SingleRowSelection,
        MultiRowsSelection
    }
    property alias selectionMode: tableView.selectionMode

    readonly property alias rows: tableView.rows
    readonly property alias columns: tableView.columns

    function setColumnWidth(index, width) {
        if (index < 0 || index >= tableView.columns || width < minimumColumnWidth)
            return
        columnWidthsRatios[index] = width / scrollView.innerWidth
        columnWidthsRatiosChanged()
    }

    onColumnWidthsRatiosChanged: tableView.forceLayout()
    onSort: tableView.updateCellsIsSelected()

    activeFocusOnTab: true
    clip: true

    Kirigami.Theme.inherit: false
    Kirigami.Theme.colorSet: Kirigami.Theme.View

    HorizontalHeaderView {
        id: horizontalHeader
        syncView: tableView

        Kirigami.Theme.inherit: false
        Kirigami.Theme.colorSet: Kirigami.Theme.Button

        delegate: StylePrivate.StyleItem {
            id: headerItem
            enabled: width > 0

            elementType: "header"
            activeControl: sortIndicatorVisible
                           && sortColumn == index ? (sortOrder == Qt.DescendingOrder ? "down" : "up") : ""
            raised: false
            sunken: mouse.pressed || activeFocus
            text: model.display != undefined ? model.display : ""
            hover: mouse.containsMouse

            focus: true

            property string headerPosition: {
                if (columns === 1) {
                    return "only"
                }
                if (index === 0) {
                    return LayoutMirroring.enabled ? "end" : "beginning"
                }
                return "middle"
            }

            properties: {
                "headerpos": headerPosition,
                "textalignment": Text.AlignHCenter
            }

            Keys.onPressed: {
                switch (event.key) {
                case Qt.Key_Space:
                case Qt.Key_Enter:
                case Qt.Key_Return:
                    toggleSort()
                    break
                case Qt.Key_Menu:
                    headerContextMenuRequested(index, mapToGlobal(headerItem.x,
                                                                  headerItem.y + headerItem.height))
                    break
                case Qt.Key_Left:
                    focusNext(-1)
                    break
                case Qt.Key_Right:
                    focusNext(1)
                    break
                default:
                    break
                }
            }

            function toggleSort() {
                if (sortColumn === index) {
                    sortOrder = sortOrder === Qt.AscendingOrder ? Qt.DescendingOrder : Qt.AscendingOrder
                } else {
                    sortColumn = index
                }
                sort(sortColumn, sortOrder)
            }

            MouseArea {
                id: mouse
                anchors.fill: parent
                hoverEnabled: true
                acceptedButtons: Qt.LeftButton | Qt.RightButton

                onClicked: mouse => {
                               if (mouse.button == Qt.RightButton) {
                                   headerContextMenuRequested(index, mapToGlobal(mouse.x, mouse.y))
                                   return
                               }
                               if (sortIndicatorVisible) {
                                   headerItem.toggleSort()
                               }
                           }
            }
            MouseArea {
                id: dragHandle
                property real mouseDownX
                property bool dragging: false
                anchors {
                    //right: !dragging ? parent.right : undefined
                    top: parent.top
                    bottom: parent.bottom
                    left: LayoutMirroring.enabled && !dragging ? parent.left : undefined
                    right: !LayoutMirroring.enabled && !dragging ? parent.right : undefined
                }
                drag.target: dragHandle
                drag.axis: Drag.XAxis
                cursorShape: enabled ? Qt.SplitHCursor : undefined
                width: Kirigami.Units.smallSpacing * 2
                onPressed: {
                    dragging = true
                    mouseDownX = x
                }
                onXChanged: {
                    if (!dragging) {
                        return
                    }
                    resize(index, headerItem.width + (x - mouseDownX))
                    setColumnWidth(index, headerItem.width + x - mouseDownX)
                    mouseDownX = x
                }
                onReleased: {
                    dragging = false
                }
            }
        }
    }
    ScrollView {
        id: scrollView
        anchors.fill: parent
        anchors.topMargin: horizontalHeader.height

        property real innerWidth: width - rightPadding

        //property real innerWidth: LayoutMirroring.enabled ? width - leftPadding : width - rightPadding
        background: Rectangle {
            color: Kirigami.Theme.backgroundColor
            Kirigami.Theme.colorSet: Kirigami.Theme.View
        }

        TableView {
            id: tableView
            clip: true
            boundsBehavior: Flickable.StopAtBounds
            activeFocusOnTab: true

            // FIXME Until Tableview correctly reverses its columns, see QTBUG-90547
            model: root.model

            Binding on model {
                when: scrollView.LayoutMirroring.enabled
                value: Table.ReverseColumnsProxyModel {
                    sourceModel: root.model
                }
            }
            property ItemSelectionModel selectionModel: ItemSelectionModel {
                id: selectionModel
                model: tableView.model
            }
            property int hoveredRow: -1
            property int sortColumn: root.sortColumn
            property int selectionMode

            signal modelLayoutHasChanged
            signal updateCellsIsSelected

            signal contextMenuRequested(var index, point position)
            onContextMenuRequested: (index, position) => root.contextMenuRequested(index, position)

            signal doubleClicked(var index)
            onDoubleClicked: root.doubleClicked(index)

            onWidthChanged: forceLayout()

            Keys.onPressed: event => {
                                switch (event.key) {
                                    case Qt.Key_Up:
                                    selectRelative(-1)
                                    if (!atYBeginning) {
                                        contentY -= root.rowHeight
                                        returnToBounds()
                                    }
                                    event.accepted = true
                                    return
                                    case Qt.Key_Down:
                                    selectRelative(1)
                                    if (!atYEnd) {
                                        contentY += root.rowHeight
                                        returnToBounds()
                                    }
                                    event.accepted = true
                                    return
                                    case Qt.Key_PageUp:
                                    if (!atYBeginning) {
                                        if ((contentY - (tableView.height - root.rowHeight)) < 0) {
                                            contentY = 0
                                        } else {
                                            contentY -= tableView.height
                                            - root.rowHeight // subtracting root.rowHeight so the last row still visible
                                        }
                                        returnToBounds()
                                    }
                                    return
                                    case Qt.Key_PageDown:
                                    if (!atYEnd) {
                                        if ((contentY + (tableView.height - root.rowHeight)) > contentHeight - height) {
                                            contentY = contentHeight - height
                                        } else {
                                            contentY += tableView.height
                                            - root.rowHeight // subtracting root.rowHeight so the last row still visible
                                        }
                                        returnToBounds()
                                    }
                                    return
                                    case Qt.Key_Home:
                                    if (!atYBeginning) {
                                        contentY = 0
                                        returnToBounds()
                                    }
                                    return
                                    case Qt.Key_End:
                                    if (!atYEnd) {
                                        contentY = contentHeight - height
                                        returnToBounds()
                                    }
                                    return
                                    case Qt.Key_Menu:
                                    contextMenuRequested(selectionModel.currentIndex,
                                                         mapToGlobal(0, 0))
                                    return
                                    default:
                                    break
                                }
                                if (root.selectionMode === Table.TableView.MultiRowsSelection) {
                                    if (event.matches(StandardKey.SelectAll)) {
                                        selectionModel.select(
                                            model.index(0, 0),
                                            ItemSelectionModel.ClearAndSelect | ItemSelectionModel.Columns)
                                        return
                                    }
                                }
                            }

            onActiveFocusChanged: {
                if (activeFocus && !selectionModel.hasSelection) {
                    selectionModel.setCurrentIndex(model.index(0, 0),
                                                   ItemSelectionModel.ClearAndSelect)
                }
            }

            function selectRelative(delta) {
                var nextRow = selectionModel.currentIndex.row + delta
                if (nextRow < 0) {
                    nextRow = 0
                }
                if (nextRow >= rows) {
                    nextRow = rows - 1
                }
                var index = model.index(nextRow, selectionModel.currentIndex.column)
                selectionModel.setCurrentIndex(
                            index, ItemSelectionModel.ClearAndSelect | ItemSelectionModel.Rows)
            }

            columnWidthProvider: function (index) {
                var widthRatio = root.columnWidthsRatios[index]
                //var widthRatio = scrollView.LayoutMirroring.enabled ? root.columnWidthsRatios[root.columnWidthsRatios.length - index - 1] : root.columnWidthsRatios[index]
                return Math.max(Math.floor(widthRatio * scrollView.innerWidth),
                                root.minimumColumnWidth)
            }
            rowHeightProvider: index => root.rowHeight
        }
    }
}
