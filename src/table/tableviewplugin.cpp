/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "tableviewplugin.h"

#include <QQmlEngine>

#include "ReverseColumnsProxyModel.h"

void TablePlugin::registerTypes(const char *uri)
{
    qmlRegisterType<ReverseColumnsProxyModel>(uri, 1, 0, "ReverseColumnsProxyModel");
    qmlRegisterModule(uri, 1, 0);
}
