import QtQuick 2.12
import QtQuick.Controls 2.2

import org.kde.kirigami 2.2 as Kirigami

AbstractTableItem {
    id: delegate

    leftPadding: Kirigami.Units.smallSpacing

    required property string text

    contentItem: Label {
        id: label
        text: delegate.text
        maximumLineCount: 1
        elide: Text.ElideRight
        horizontalAlignment: Text.AlignHCenter
    }

    ToolTip.text: delegate.text
    ToolTip.delay: Kirigami.Units.toolTipDelay
    ToolTip.visible: hovered && label.truncated
}
