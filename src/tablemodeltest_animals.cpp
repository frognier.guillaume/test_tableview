#include "tablemodeltest_animals.h"

TableModelTestAnimals::TableModelTestAnimals(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QVariant TableModelTestAnimals::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation);
    Q_UNUSED(role);
    return m_headerList[section];
}

int TableModelTestAnimals::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_rowCount;
}

int TableModelTestAnimals::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_columnCount;
}

QVariant TableModelTestAnimals::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role < Qt::UserRole)
        return m_dataList[index.row() * m_columnCount + index.column()];
    else
        return m_dataList[index.row() * m_columnCount + role - Qt::UserRole];
}

QHash<int, QByteArray> TableModelTestAnimals::roleNames() const
{
    return {
        {Qt::DisplayRole, "display"},
        {TableModelTestAnimalsRole::Animal, "animal"},
        {TableModelTestAnimalsRole::Color, "color"},
        {TableModelTestAnimalsRole::Age, "age"},
        {TableModelTestAnimalsRole::Mass, "mass"},
        {TableModelTestAnimalsRole::Habitat, "habitat"},
        {TableModelTestAnimalsRole::Height, "height"},
        {TableModelTestAnimalsRole::Diet, "diet"},
        {TableModelTestAnimalsRole::Speed, "speed"}};
}
