#include <QQmlApplicationEngine>
#include <QtQml>

#include <QApplication>

#include "tablemodeltest.h"
#include "tablemodeltest_animals.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<TableModelTest>("test", 1, 0, "TableModelTest");

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }
    return a.exec();
}
