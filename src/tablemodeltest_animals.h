#ifndef TABLEMODELTEST_ANIMALS_H
#define TABLEMODELTEST_ANIMALS_H

#include <QAbstractTableModel>
#include <QString>

class TableModelTestAnimals : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TableModelTestAnimals(QObject *parent = nullptr);

    enum TableModelTestAnimalsRole {
        Animal = Qt::UserRole,
        Color,
        Age,
        Mass,
        Habitat,
        Diet,
        Height,
        Speed
    };
    Q_ENUM(TableModelTestAnimalsRole)

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    static const int m_columnCount = 8;
    static const int m_rowCount = 36;
    const QString m_headerList[m_columnCount] = {
        QLatin1String("animal"),
        QLatin1String("color"),
        QLatin1String("age"),
        QLatin1String("mass"),
        QLatin1String("habitat"),
        QLatin1String("diet"),
        QLatin1String("height"),
        QLatin1String("speed")};
    // clang-format off
    const QString m_dataList[m_columnCount*m_rowCount] = {
        QLatin1String("cat"), QLatin1String("black"), QLatin1String("2.5"), QLatin1String("1"), QLatin1String("farmland"), QLatin1String("carnivore"), QLatin1String("0.20"), QLatin1String("50"),
        QLatin1String("dog"), QLatin1String("brown"), QLatin1String("8"), QLatin1String("60"), QLatin1String("farmland"), QLatin1String("carnivore"), QLatin1String("1.20"), QLatin1String("60"),
        QLatin1String("bird"), QLatin1String("red and white"), QLatin1String("0.5"), QLatin1String("0.050"), QLatin1String("farmland"), QLatin1String("herbivore"), QLatin1String("0.20"), QLatin1String("80"),
        QLatin1String("sheep"), QLatin1String("white"), QLatin1String("5"), QLatin1String("30"), QLatin1String("farmland"), QLatin1String("herbivore"), QLatin1String("1.30"), QLatin1String("20"),
        QLatin1String("chicken"), QLatin1String("ginger"), QLatin1String("2"), QLatin1String("2"), QLatin1String("farmland"), QLatin1String("herbivore"), QLatin1String("0.30"), QLatin1String("20"),
        QLatin1String("wale"), QLatin1String("blue"), QLatin1String("50"), QLatin1String("10000"), QLatin1String("ocean"), QLatin1String("carnivore"), QLatin1String("4"), QLatin1String("50"),

        QLatin1String("elephant"), QLatin1String("gray"), QLatin1String("30"), QLatin1String("5000"), QLatin1String("savanna"), QLatin1String("herbivore"), QLatin1String("2.70"), QLatin1String("60"),
        QLatin1String("tiger"), QLatin1String("orange"), QLatin1String("12"), QLatin1String("200"), QLatin1String("jungle"), QLatin1String("carnivore"), QLatin1String("1.40"), QLatin1String("90"),
        QLatin1String("horse"), QLatin1String("brown"), QLatin1String("6"), QLatin1String("800"), QLatin1String("farmland"), QLatin1String("herbivore"), QLatin1String("1.90"), QLatin1String("80"),
        QLatin1String("lion"), QLatin1String("yellow"), QLatin1String("10"), QLatin1String("250"), QLatin1String("savanna"), QLatin1String("carnivore"), QLatin1String("1.50"), QLatin1String("70"),
        QLatin1String("penguin"), QLatin1String("black and white"), QLatin1String("1"), QLatin1String("1"), QLatin1String("ocean"), QLatin1String("carnivore"), QLatin1String("0.80"), QLatin1String("20"),
        QLatin1String("giraffe"), QLatin1String("spotted"), QLatin1String("8"), QLatin1String("1200"), QLatin1String("savanna"), QLatin1String("carnivore"), QLatin1String("3.10"), QLatin1String("50"),

        QLatin1String("koala"), QLatin1String("gray"), QLatin1String("5"), QLatin1String("10"), QLatin1String("jungle"), QLatin1String("herbivore"), QLatin1String("1.10"), QLatin1String("40"),
        QLatin1String("snake"), QLatin1String("green"), QLatin1String("4"), QLatin1String("2"), QLatin1String("farmland"), QLatin1String("carnivore"), QLatin1String("0.10"), QLatin1String("60"),
        QLatin1String("bear"), QLatin1String("brown"), QLatin1String("15"), QLatin1String("800"), QLatin1String("forest"), QLatin1String("carnivore"), QLatin1String("2.20"), QLatin1String("70"),
        QLatin1String("panda"), QLatin1String("black and white"), QLatin1String("8"), QLatin1String("100"), QLatin1String("jungle"), QLatin1String("herbivore"), QLatin1String("1.90"), QLatin1String("40"),
        QLatin1String("rhinoceros"), QLatin1String("gray"), QLatin1String("40"), QLatin1String("2000"), QLatin1String("savanna"), QLatin1String("herbivore"), QLatin1String("1.90"), QLatin1String("70"),
        QLatin1String("octopus"), QLatin1String("purple"), QLatin1String("3"), QLatin1String("5"), QLatin1String("ocean"), QLatin1String("carnivore"), QLatin1String("0.50"), QLatin1String("50"),
        QLatin1String("kangaroo"), QLatin1String("brown"), QLatin1String("7"), QLatin1String("50"), QLatin1String("savanna"), QLatin1String("herbivore"), QLatin1String("1.90"), QLatin1String("70"),
        QLatin1String("dolphin"), QLatin1String("gray"), QLatin1String("15"), QLatin1String("200"), QLatin1String("ocean"), QLatin1String("carnivore"), QLatin1String("0.90"), QLatin1String("70"),

        QLatin1String("cheetah"), QLatin1String("yellow and black"), QLatin1String("8"), QLatin1String("100"), QLatin1String("savanna"), QLatin1String("carnivore"), QLatin1String("1.20"), QLatin1String("110"),
        QLatin1String("squirrel"), QLatin1String("gray"), QLatin1String("2"), QLatin1String("0.5"), QLatin1String("farmland"), QLatin1String("herbivore"), QLatin1String("0.20"), QLatin1String("50"),
        QLatin1String("crocodile"), QLatin1String("green"), QLatin1String("30"), QLatin1String("500"), QLatin1String("lake"), QLatin1String("carnivore"), QLatin1String("0.50"), QLatin1String("60"),
        QLatin1String("polar bear"), QLatin1String("white"), QLatin1String("20"), QLatin1String("800"), QLatin1String("drift ice"), QLatin1String("carnivore"), QLatin1String("2.20"), QLatin1String("50"),
        QLatin1String("parrot"), QLatin1String("green"), QLatin1String("4"), QLatin1String("0.2"), QLatin1String("jungle"), QLatin1String("herbivore"), QLatin1String("0.60"), QLatin1String("80"),
        QLatin1String("zebra"), QLatin1String("black and white"), QLatin1String("10"), QLatin1String("500"), QLatin1String("savanna"), QLatin1String("herbivore"), QLatin1String("1.60"), QLatin1String("50"),
        QLatin1String("gazelle"), QLatin1String("tan"), QLatin1String("6"), QLatin1String("75"), QLatin1String("savanna"), QLatin1String("herbivore"), QLatin1String("1.50"), QLatin1String("80"),
        QLatin1String("elephant seal"), QLatin1String("gray"), QLatin1String("12"), QLatin1String("1500"), QLatin1String("ocean"), QLatin1String("carnivore"), QLatin1String("0.80"), QLatin1String("20"),

        QLatin1String("puma"), QLatin1String("tan"), QLatin1String("9"), QLatin1String("150"), QLatin1String("mountains"), QLatin1String("carnivore"), QLatin1String("1.60"), QLatin1String("70"),
        QLatin1String("gibbon"), QLatin1String("black"), QLatin1String("12"), QLatin1String("30"), QLatin1String("jungle"), QLatin1String("carnivore"), QLatin1String("1.20"), QLatin1String("50"),
        QLatin1String("leopard"), QLatin1String("yellow and black"), QLatin1String("6"), QLatin1String("70"), QLatin1String("jungle"), QLatin1String("carnivore"), QLatin1String("1.50"), QLatin1String("60"),
        QLatin1String("sea otter"), QLatin1String("brown"), QLatin1String("10"), QLatin1String("20"), QLatin1String("farmland"), QLatin1String("carnivore"), QLatin1String("0.20"), QLatin1String("50"),
        QLatin1String("hippopotamus"), QLatin1String("gray"), QLatin1String("35"), QLatin1String("2500"), QLatin1String("farmland"), QLatin1String("carnivore"), QLatin1String("0.20"), QLatin1String("50"),
        QLatin1String("peacock"), QLatin1String("blue and green"), QLatin1String("5"), QLatin1String("5"), QLatin1String("farmland"), QLatin1String("carnivore"), QLatin1String("0.20"), QLatin1String("50"),
        QLatin1String("raccoon"), QLatin1String("gray"), QLatin1String("4"), QLatin1String("15"), QLatin1String("farmland"), QLatin1String("carnivore"), QLatin1String("0.20"), QLatin1String("50"),
        QLatin1String("platypus"), QLatin1String("brown"), QLatin1String("3"), QLatin1String("2"), QLatin1String("farmland"), QLatin1String("carnivore"), QLatin1String("0.20"), QLatin1String("50")
    };
    // clang-format on
};

#endif // TABLEMODELTEST_ANIMALS_H
