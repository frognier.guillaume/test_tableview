import QtQuick 2.15
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Controls 2.15 as QQC2

import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.3

import test 1.0
import org.kde.kitemmodels 1.0 as KItemModels

import org.kde.testing.tableview 1.0 as Table

Kirigami.Page {

    Flickable {
        Table.TableView {
            anchors.fill: parent
            id: tableView
            clip: true
            model: filteredTableModel
            delegate: Table.TextTableItem {
                text: display
                implicitWidth: 100
                implicitHeight: 50
            }
            onSortIndicatorVisibleChanged: showPassiveNotification(
                                               "sortColumn: %1 sortOrder: %2 sortIndicatorVisible: %3".arg(
                                                   sortColumn).arg(sortOrder).arg(
                                                   sortIndicatorVisible))
            onSort: (column, order) => showPassiveNotification("sort(column: %1, order: %2)".arg(
                                                                   column).arg(order))
            onResize: (column, width) => showPassiveNotification(
                          "resize(column: %1, width: %2)".arg(column).arg(width))
            onContextMenuRequested: (index, position) => showPassiveNotification(
                                        "contextMenuRequested(index: %1, position: %2)".arg(
                                            index).arg(position))
            onHeaderContextMenuRequested: (column, position) => showPassiveNotification(
                                              "headerContextMenuRequested(column: %1, position: %2)".arg(
                                                  column).arg(position))
        }
    }

    KItemModels.KSortFilterProxyModel {
        id: filteredTableModel
        sourceModel: TableModelTest {}
        sortOrder: tableView.sortIndicatorVisible ? tableView.sortOrder : Qt.AscendingOrder
        sortColumn: tableView.sortIndicatorVisible ? tableView.sortColumn : -1
        sortCaseSensitivity: Qt.CaseInsensitive
    }
}
