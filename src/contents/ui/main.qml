import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.19 as Kirigami
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.3

import test 1.0
import org.kde.kitemmodels 1.0 as KItemModels

import org.kde.testing.tableview 1.0 as Table

Kirigami.ApplicationWindow {
    id: root
    title: "Testing"

    pageStack.initialPage: pageOneComponent
    Component {
        id: pageOneComponent
        PageOne {
            id: pageOne
        }
    }
    Component {
        id: pageTwoComponent
        PageTwo {
            id: pageTwo
        }
    }
    Component {
        id: pageThreeComponent
        PageThree {
            id: pageThree
        }
    }

    header: Kirigami.ActionToolBar {
        actions: [
            Kirigami.Action {
                id: actionPageSelection
                displayComponent: QQC2.ComboBox {
                    id: comboBox
                    textRole: "title"
                    valueRole: "page"
                    model: [{
                            "title": "page 1",
                            "page": pageOneComponent
                        }, {
                            "title": "page 2",
                            "page": pageTwoComponent
                        }, {
                            "title": "page 3",
                            "page": pageThreeComponent
                        }]
                    onCurrentValueChanged: {
                        pageStack.pop()
                        pageStack.push(currentValue)
                    }
                }
            }
        ]
    }
}
