import QtQuick 2.15
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Controls 1.4 as QQC1

import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.3

import test 1.0
import org.kde.kitemmodels 1.0 as KItemModels

Kirigami.Page {

    QQC1.TableView {
        id: tableView
        anchors.fill: parent

        model: filteredTableModel

        Component {
            id: columnComponent
            QQC1.TableViewColumn {
                title: ""
                role: ""
            }
        }

        function addColumns() {
            var i
            for (i = 0; i < tableModelTest.columnCount(); i++) {
                tableView.addColumn(columnComponent.createObject(tableView, {
                                                                     "title": "Section %1".arg(i),
                                                                     "role": "r%1".arg(i)
                                                                 }))
            }
        }

        Component.onCompleted: addColumns()
    }

    TableModelTest {
        id: tableModelTest
    }

    KItemModels.KSortFilterProxyModel {
        id: filteredTableModel
        sourceModel: tableModelTest
        sortOrder: tableView.sortIndicatorVisible ? tableView.sortOrder : Qt.AscendingOrder
        sortColumn: tableView.sortIndicatorVisible ? tableView.sortColumn : -1
        sortCaseSensitivity: Qt.CaseInsensitive
    }
}
