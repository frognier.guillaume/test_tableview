import QtQuick 2.15
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Controls 1.4 as QQC1

import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.3

import test 1.0
import org.kde.kitemmodels 1.0 as KItemModels

Kirigami.Page {

    QQC1.TableView {
        anchors.fill: parent
        id: tableView
        model: filteredTableModel
        QQC1.TableViewColumn {
            title: "Animal"
            role: "animal"
        }
        QQC1.TableViewColumn {
            title: "Color"
            role: "color"
        }
        QQC1.TableViewColumn {
            title: "Age"
            role: "age"
        }
        QQC1.TableViewColumn {
            title: "Mass"
            role: "mass"
        }
        QQC1.TableViewColumn {
            title: "Habitat"
            role: "habitat"
        }
        QQC1.TableViewColumn {
            title: "Diet"
            role: "diet"
        }
        QQC1.TableViewColumn {
            title: "Height"
            role: "height"
        }
        QQC1.TableViewColumn {
            title: "Speed"
            role: "speed"
        }
    }

    KItemModels.KSortFilterProxyModel {
        id: filteredTableModel
        sourceModel: TableModelTest {}
        sortOrder: tableView.sortIndicatorVisible ? tableView.sortOrder : Qt.AscendingOrder
        sortColumn: tableView.sortIndicatorVisible ? tableView.sortColumn : -1
        sortCaseSensitivity: Qt.CaseInsensitive
    }
}
