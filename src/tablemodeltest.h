#ifndef TABLEMODELTEST_H
#define TABLEMODELTEST_H

#include <QAbstractTableModel>
#include <QString>

class TableModelTest : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TableModelTest(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:

    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    Q_INVOKABLE int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    static const int m_columnCount = 1500;
    static const int m_rowCount = 1500;
};

#endif // TABLEMODELTEST_H
