#include "tablemodeltest.h"

TableModelTest::TableModelTest(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QVariant TableModelTest::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation);
    Q_UNUSED(role);
    return QStringLiteral("Section %1").arg(section);
}

int TableModelTest::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_rowCount;
}

int TableModelTest::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_columnCount;
}

QVariant TableModelTest::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role < Qt::UserRole)
        return QStringLiteral("(%1,%2)").arg(index.row()).arg(index.column());
    else
        return QStringLiteral("(%1,%2)").arg(index.row()).arg(role - Qt::UserRole);
}

QHash<int, QByteArray> TableModelTest::roleNames() const
{
    QHash<int, QByteArray> hash = QAbstractTableModel::roleNames();

    for (int i = 0; i < m_columnCount; i++) {
        hash.insert(i + Qt::UserRole, "r" + QByteArray::number(i));
    }

    return hash;
}
